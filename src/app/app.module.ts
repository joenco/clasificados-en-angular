import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';
import { APP_ROUTER } from './app.routes';
import { ActionReducerMap } from '@ngrx/store'; 
import { StoreModule as NgRxStoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppComponent } from './app.component';
import { ListaClasificadosComponent } from './components/lista-clasificados/lista-clasificados.component';
import { ClasificadoComponent } from './components/clasificado/clasificado.component';
import { ClasificadodetalleComponent } from './components/clasificadodetalle/clasificadodetalle.component';
import { FormClasificadoComponent } from './components/form-clasificado/form-clasificado.component';
import { ClasificadosApiClient } from './models/clasificados-api-client.model';
import { 
  ClasificadosState,
  initializeClasificadosState,
  reducerClasificados,
  ClasificadosEffects
} from './models/clasificados-state.model';

export interface AppState {
  //estado global de la operacion, compuesto por los distintos features
  clasificados: ClasificadosState;
}

const reducers: ActionReducerMap<AppState> = {
  clasificados: reducerClasificados
}
//inicializacion
let reducersInitialState = {
  clasificados: initializeClasificadosState()
}
//redux fin init

@NgModule({
  declarations: [
    AppComponent,
    ListaClasificadosComponent,
    ClasificadoComponent,
    ClasificadodetalleComponent,
    FormClasificadoComponent
  ],

  imports: [
    BrowserModule,
    APP_ROUTER,
    FormsModule,
    ReactiveFormsModule,
    NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState }),
    EffectsModule.forRoot([ClasificadosEffects]),
    StoreDevtoolsModule.instrument(),
    HttpModule
  ],

  providers: [
    ClasificadosApiClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
