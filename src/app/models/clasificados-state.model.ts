import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Clasificados } from './clasificados.model';

//ESTADO
export interface ClasificadosState{
    items: Clasificados[];
    loading: boolean;
    favorito: Clasificados;
}

export const initializeClasificadosState = function(){
   return {
      items: [],
      loading: false,
      favorito: null
    }
}

//ACCIONES
export enum ClasificadosActionTypes {
  NUEVO_DESTINO = '[Clasificados] Nuevo',
  ELEGIDO_FAVORITO = '[Clasificados] Favorito',
  VOTE_UP = '[Clasificados] Vote Up',
  VOTE_DOWN = '[Clasificados] Vote Down'
}

export class NuevoClasificadoAction implements Action {
  type = ClasificadosActionTypes.NUEVO_CLASIFICADO;
  constructor(public clasificado: Clasificados) {}
}

export class ElegidoFavoritoAction implements Action {
  type = ClasificadosActionTypes.ELEGIDO_FAVORITO;
  constructor(public clasificado: Clasificados) {}
}

export class VoteUpAction implements Action{
  type = ClasificadosActionTypes.VOTE_UP;
  constructor(public clasificado: Clasificados) {}
}

export class VoteDownAction implements Action{
  type = ClasificadosActionTypes.VOTE_DOWN;
  constructor(public clasificado: Clasificados) {}
}

//union de tipos
export type ClasificadosActions = NuevoClasificadoAction | ElegidoFavoritoAction
  | VoteUpAction | VoteDownAction;

//REDUCERS
export function reducerClasificados(
  state:ClasificadosState,
  action:ClasificadosActions
) : ClasificadosState {
switch (action.type) {
  case ClasificadosActionTypes.NUEVO_CLASIFICADO: {
    return {
      ...state,
      items: [...state.items, (action as NuevoClasificadoAction).clasificado ]
    };
  }
  case ClasificadosActionTypes.ELEGIDO_FAVORITO: {
    state.items.forEach(x => x.setSelected(false));
    let fav:Clasificados = (action as ElegidoFavoritoAction).clasificado;
    fav.setSelected(true);
    return {
      ...state,
      favorito: fav
    };
  }
  case ClasificadosActionTypes.VOTE_UP: {
    const c: Clasificados = (action as VoteUpAction).clasificado;
    c.voteUp();
    return{ ...state };
  }
  case ClasificadosActionTypes.VOTE_DOWN: {
    const c: Clasificados = (action as VoteDownAction).clasificado;
    c.voteDown();
    return{ ...state };
  }
}
return state;
}

//EFFECTS
@Injectable()
export class ClasificadosEffects {
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
  //cada vez que crean un clasificado nuevo lo marco como nuevo favorito
    ofType(ClasificadosActionTypes.NUEVO_CLASIFICADO),
      map((action:NuevoClasificadoAction) => new ElegidoFavoritoAction(action.clasificado))
  );

  constructor(private actions$: Actions) {}
}

