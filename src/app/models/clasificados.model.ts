export class Clasificados {

  titulo:string;
  descripcion:string;
  url:string;
  urlImagen:string;
  constructor(public t:string, public d:string, public u:string, i:string) {
    this.titulo = t;
    this.descripcion = d;
    this.url = u;
    this.urlImagen = i;
  }

}
