import { Clasificados } from './clasificados.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState} from '../app.module';
import { NuevoClasificadoAction, ElegidoFavoritoAction } from './clasificados-state.model';
import { Injectable } from '@angular/core';

@Injectable()
export class ClasificadosApiClient {

  constructor(private store: Store<AppState>) {
  }

  add(c: Clasificados){
    this.store.dispatch(new NuevoClasificadoAction(c));
  }

  elegir(c: Clasificados){
    this.store.dispatch(new ElegidoFavoritoAction(c));
  }

}

