import { RouterModule, Routes} from '@angular/router';
import { ListaClasificadosComponent} from './components/lista-clasificados/lista-clasificados.component';
import { ClasificadodetalleComponent } from './components/clasificadodetalle/clasificadodetalle.component';

const APPROUTES: Routes = [
    { path: 'home', component: ListaClasificadosComponent },
    { path: 'clasificado', component: ClasificadodetalleComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'home'}
];

// tslint:disable-next-line:eofline
export const APP_ROUTER = RouterModule.forRoot(APPROUTES);
