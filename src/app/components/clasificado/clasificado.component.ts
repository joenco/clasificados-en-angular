import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import {Clasificados} from '../../models/clasificados.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import{ VoteUpAction, VoteDownAction } from '../../models/clasificados-state.model';

@Component({
  selector: 'app-clasificado',
  templateUrl: './clasificado.component.html',
  styleUrls: ['./clasificado.component.css']
})
export class ClasificadoComponent implements OnInit {
  @Input() clasificado:Clasificados;
  @Input("idx") position: number;
  @HostBinding('attr.class') cssClass = 'col-md-12';
  @Output() onClicked: EventEmitter<Clasificados>;
  constructor(private store: Store<AppState>) {
    this.onClicked = new EventEmitter();
  }

  ngOnInit() {
  }

  destacado() {
    this.onClicked.emit(this.clasificado);
    return false;
  }

  voteUp(){
    this.store.dispatch(new VoteUpAction(this.clasificado));
    return false;
  }

  voteDown(){
    this.store.dispatch(new VoteDownAction(this.clasificado));
    return false;
  }

}
