import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Clasificados} from '../../models/clasificados.model';
import { ClasificadosApiClient } from './../../models/clasificados-api-client.model';
import { Store }  from '@ngrx/store';
import { AppState }  from '../../app.module';
import { ElegidoFavoritoAction, NuevoClasificadoAction } from '../../models/clasificados-state.model';

@Component({
  selector: 'app-lista-clasificados',
  templateUrl: './lista-clasificados.component.html',
  styleUrls: ['./lista-clasificados.component.css']
})
export class ListaClasificadosComponent implements OnInit {
  clasificados:Clasificados[];
  @Output() onItemAdded: EventEmitter<Clasificados>;
  updates:string[];
  all;
  constructor(private clasificadosApiClient: ClasificadosApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.clasificados.favorito)
      .subscribe(c => {
        if(c != null){
          this.updates.push('Se ha elegido a ' + c.titulo);
        }
      });
    store.select(state => state.clasificados.items).subscribe(items => this.all = items);
  }

  ngOnInit() {
  }

  agregado(c: Clasificados) {
    this.clasificadosApiClient.add(c);
    this.onItemAdded.emit(c);
  }

  elegido(c: Clasificados) {
    this.clasificadosApiClient.elegir(c);
  }

}  
