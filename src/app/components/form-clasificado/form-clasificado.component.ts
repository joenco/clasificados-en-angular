import { Component, OnInit, EventEmitter, Output, Inject, forwardRef } from '@angular/core';
import { Clasificados } from './../../models/clasificados.model';
import { FormBuilder, FormGroup, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';

@Component({
  selector: 'app-form-clasificado',
  templateUrl: './form-clasificado.component.html',
  styleUrls: ['./form-clasificado.component.css']
})
export class FormClasificadoComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<Clasificados>;
  fg: FormGroup;
  minLongitud = 6;
  searchResults: string[];
  constructor(fb: FormBuilder) {
    this.onItemAdded = new EventEmitter();

    this.fg = fb.group({
        titulo: ['', Validators.compose([
            Validators.required,
            this.tituloValidator,
            this.tituloValidatorParametrizable(this.minLongitud)
        ])],
        descripcion: ['', Validators.compose([
            Validators.required,
            this.descripcionValidator,
            this.descripcionValidatorParametrizable(this.minLongitud)
        ])],
        url: [''],
        imagen: ['']
    });

  }

  ngOnInit() {
    let elemTitulo = <HTMLInputElement>document.getElementById('titulo');
    fromEvent(elemTitulo, 'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 4),
        debounceTime(200),
        distinctUntilChanged(),
      switchMap(() => ajax('/assets/datos.json'))
    ).subscribe(ajaxResponse => {
      console.log(ajaxResponse.response);
      this.searchResult = ajaxResponse.response;
    });
  }

  publicar(titulo: string, descripcion: string, url: string, imagen: string): boolean {
    const c = new Clasificados(titulo, descripcion, url, imagen);
    this.onItemAdded.emit(c);
    return false;
  }

  tituloValidator(control: FormControl): { [s: string]: boolean } {
    const l = control.value.toString().trim().length;
    if (l > 0 && l < 6) {
      return {invalidTitulo: true};
    }
    return null;
  }

  tituloValidatorParametrizable(minLong: number): ValidatorFn {
      return (control: FormControl): { [key: string]: boolean } | null => {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < minLong) {
        return { 'minLongNombre': true };
      }
      return null;
    };
  }

  descripcionValidator(control: FormControl): { [s: string]: boolean } {
    const l = control.value.toString().trim().length;
    if (l > 0 && l < 6) {
      return {invalidDescipcion: true};
    }
    return null;
  }

  descripcionValidatorParametrizable(minLong: number): ValidatorFn {
      return (control: FormControl): { [key: string]: boolean } | null => {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < minLong) {
        return { 'minLongNombre': true };
      }
      return null;
    };
  }

}
