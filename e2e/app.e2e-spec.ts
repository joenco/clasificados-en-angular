import { ClasificadosAngularPage } from './app.po';

describe('clasificados-angular App', function() {
  let page: ClasificadosAngularPage;

  beforeEach(() => {
    page = new ClasificadosAngularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
